# ZSH

## Install necessary fonts

> TODO: Check if this step can be automated

Manually install fonts, then open Gnome Terminal → Preferences and click on the selected profile under Profiles. Check Custom font under Text Appearance and select MesloLGS NF. Then run `make install`. You might have to log in and out again to automatically switch from `bash` to `zsh` when opening a terminal. 