# Neovim

Automatically install and configure neovim as your IDE on Ubuntu.

## Manual steps

### Configure nvim

After running `make install`, create your `init.vim` (user config) file.
Inside nvim run:

```
    :call mkdir(stdpath('config'), 'p')
    :exe 'edit '.stdpath('config').'/init.vim'
```

Add these contents to the file:

```
    set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc
```

then `:wq` out of the file and restart nvim.
