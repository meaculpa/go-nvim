install:
		sudo apt install curl
		cd neovim && make install && cd -
		cd golang && make install && cd -
		cd zsh && make install && cd -